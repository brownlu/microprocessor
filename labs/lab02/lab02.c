#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.
/**
 * @brief LAB #02 - TEMPLATE
 *        Main entry point for the code.
 * 
 * @return int      Returns exit-status zero on completion.
 */
//Wallis Product function single precision
double WallisSingle(int x)
{
    //initialise variables
    float prod=1;
    float numerator =2;
    float firstDenominator =1;
    float secondDenominator=3;

for(int count =0; count <x; count++)
{
    float tempProd = (numerator/firstDenominator)*(numerator/secondDenominator); //calculate current product e.g. (2/1 * 2/3)
    //printf("Temp prod = %f\n",tempProd);

    //increment variables    
    firstDenominator = secondDenominator;
    secondDenominator+=2;
    numerator+=2;

    //multiply answer by calculated product
    prod *=tempProd;
    //printf("Prod = %f\n",prod);

}
return prod;
}
// Wallis Product function double precision
double WallisDouble(int x)
{
    //initialise variables
    double prod=1;
    double numerator =2;
    double firstDenominator =1;
    double secondDenominator=3;

for(int count =0; count <x; count++)
{
    double tempProd = (numerator/firstDenominator)*(numerator/secondDenominator); //calculate current product e.g. (2/1 * 2/3)
    //printf("Temp prod = %f\n",tempProd);

    //increment variables    
    firstDenominator = secondDenominator;
    secondDenominator+=2;
    numerator+=2;

    //multiply answer by calculated product
    prod *=tempProd;
    //printf("Prod = %f\n",prod);
}
return prod;
}
int main() {
    stdio_init_all();
    
    //call function 100000 times for accuracy
    printf("Wallis Single Precision: ");
    float wallisProductSingle = WallisSingle(100000);
    //pi = wallis * 2
    float piSingle = wallisProductSingle * 2;
    //print final value of pi
    printf("Pi_Single = %f\n\n",piSingle);

    //call function 100000 times for accuracy
    printf("Wallis Double Precision: ");
    double wallisProductDouble = WallisDouble(100000);
    //pi = wallis * 2
    double piDouble = wallisProductDouble * 2;
    //print final value of pi
    printf("Pi_Double = %f\n\n",piDouble);

    double comparisonValue = 3.14159265359;
    //calculate and print percentage error
    double percentageError = (1-(piDouble/comparisonValue))*100;
    printf("Approx Error = %f percent \n\n",percentageError);

    // Returning zero indicates everything went okay.
    return 0;
}
